import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { Chart } from 'chart.js';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements AfterViewInit {

  public devices = [];
  public connectedDevice = null;
  public btEnabled;
  public connected;
  public deviceData = [];
  public archiveData = [];
  public chartData = [];
  public dataRetrivalInterval;
  public showLevelSettings = false;
  public showPeriodicSettings = false;
  public configLevel = 0;
  private _loader;

  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any = null;

  constructor(
    private bluetoothSerial: BluetoothSerial,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController
  ) {
    this.connected = false;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad");

    this.checkIfBTEnabled();
    if( this.btEnabled == true ) {
      this.disconnect();
      this._loadDevices(); 
    } else {
      this.enableBt(); 
    }
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
    console.log(this.lineCanvas.nativeElement);
  }

  public showCredits() {
      let creditsAlert = this.alertCtrl.create({
        title: 'Z okazji urodzin :)',
        subTitle: 'Wszystkiego najlepszego, niech Ci kwiaty rosną ! Najlepszy brat, Piotrek :)',
        buttons: ['OK']
      });
      creditsAlert.present();
  }
  
  public disconnect() {
    this.connectedDevice = null;
    this.connected = false;
    this.deviceData = [];
    this.stopGettingDataFromDevice();
    this.chartData = [];

    this.bluetoothSerial.disconnect().then(data => {
      console.log("Disconnected");
    }, err => {
      console.log("Error durning disconnecting");
    });
  }

  public enableBt() {
    this.bluetoothSerial.enable().then(data => {
      console.log("Enable BT action ");
      console.log(data);
      if( data.trim() == "OK" ) {
        this.btEnabled = true; 
        this._loadDevices();
      }
    });
  }

  public deviceSelected(device) {
    console.log("Selected device:", device); 
    this._connectToDevice(device); 
  }

  public checkIfBTEnabled() {
    this.bluetoothSerial.isEnabled().then(data => {
      console.log("BT enabled? ", data);
      if( data.trim() == "OK" ) {
        this.btEnabled = true; 
      } else {
        this.btEnabled = false; 
      }
    });
  }

  public waterPlant() {
    const actionWaterPlant = this.actionSheetCtrl.create({
      title: 'Ile użyć wody ?',
      buttons: [
        {
          text: 'Trochę',
          handler: () => {
            this._sendWaterAction('WATER1');
          }
        },{
          text: 'Dosyć',
          handler: () => {
            this._sendWaterAction('WATER2');
          }
        },{
          text: 'W opór',
          handler: () => {
            this._sendWaterAction('WATER3');
          }
        },{
          text: 'Anuluj',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    }); 
    actionWaterPlant.present();
  }

  public startGettingDataFromDevice() {
    console.log("Starting to get data");
    this.bluetoothSerial.write('ON');

    setTimeout(() => {
      this.stopGettingDataFromDevice(); 
    }, 60000);

    this.dataRetrivalInterval = setInterval(() => {
      console.log("All data: ", this.deviceData);
      this.bluetoothSerial.readUntil(";").then(data => {
        console.log("Reading device data:");
        console.log(data);

        if( data != "") {

          data = data.slice(0, -1);
          let measurement_value = 0;
          if( (data[0] == "1" && data.length == 3) || data.length <= 2 ) {
            measurement_value = data;
          } else {
            measurement_value = data.slice(0, 1);
          }

          let itemRecord = {
            'date': Date.now(),
            'value': measurement_value
          }
          console.log("Adding record");
          console.log(itemRecord);

          this.deviceData.unshift(itemRecord); 
          this.deviceData = this.deviceData.slice(0, 100);
          // this.chartData = this.deviceData.map(x => parseInt(x.value)).reverse();
          // this.chartData.push(0);
          // this._renderChart();
        }
      }, err => {
        console.log("Could not read device data");
      });
    }, 1200);
  }

  public stopGettingDataFromDevice() {
    this.bluetoothSerial.write('OFF');
    console.log("Stopping to get data");

    clearInterval(this.dataRetrivalInterval);
    this.dataRetrivalInterval = null;
  }

  public downloadArchiveData() {
    this.chartData = [];
    this._getArchiveData(); 
  }

  public showSettings(settings_type) {
    console.log("selected settings: ", settings_type);

    if( settings_type == 'periodic') {
      this.showPeriodicSettings = true; 
    }

    if( settings_type == 'level') {
      this._getConfigLevel();
      this.showLevelSettings = true; 
    }
  }

  public saveConfigLevel() {
    this.bluetoothSerial.write('SET_LEVEL#'+this.configLevel).then(() => {
      console.log("New config level set: ", 'SET_LEVEL#'+this.configLevel);
      this.hideSettings();

    }, err => {
      let errAlert = this.alertCtrl.create({
        title: 'Łups',
        subTitle: 'Błąd podczas zapisywania, spróbuj jeszcze raz',
        buttons: ['OK']
      });
      errAlert.present();
    }); 
  }

  public hideSettings() {
    this.showPeriodicSettings = false; 
    this.showLevelSettings = false; 
  }

  private _getConfigLevel() {
    this._startLoader();

    this.bluetoothSerial.write('GET_LEVEL').then(() => {
      console.log("Getting current config level");

      setTimeout(() => {
        this.bluetoothSerial.readUntil(";").then(data => {
          console.log("Reading config level:");
          console.log(data);

          this.configLevel = data.split(';')[0].trim();
          console.log("Config level: ", this.configLevel);
        }, err => {
          console.log("Could not read device data");
        });
        this._stopLoader();
      }, 2000);

    }, err => {
      this._stopLoader();
      let errAlert = this.alertCtrl.create({
        title: 'Łups',
        subTitle: 'Nie mogę pobrać obecnie ustawionego poziomu, spróbuj jeszcze raz',
        buttons: ['OK']
      });
      errAlert.present();
    }); 
  }

  private _sendWaterAction(amount) {
  
    this.bluetoothSerial.write(amount).then(() => {
      console.log("Plant watered");
    }, err => {
      let errAlert = this.alertCtrl.create({
        title: 'Łups',
        subTitle: 'Coś doniczka nie zrozumiała instrukcji podlania, spróbuj jeszcze raz',
        buttons: ['OK']
      });
      errAlert.present();
    }); 
  }

  private _loadDevices() {
    this.bluetoothSerial.list().then(data => {
      console.log("BT devices:");
      console.log(data);
      this.devices = data;
    }, err => {
      console.log("Could not list the BT devices");
    });
  }

  private _connectToDevice(device) {
    console.log("In device connection...");
    this._startLoader();

    this.bluetoothSerial.connect(device.address).subscribe(data => {
      console.log("Device connection:");
      console.log(data);

      this._stopLoader();
      if( data.trim() == "OK" ) {
        this.connected = true;
        this.connectedDevice = device;
        console.log("Connected device: ");
        console.log(this.connectedDevice);
        // this._getArchiveData();
        // this.startGettingDataFromDevice();
      } else {
        this.connected = false; 
        let errAlert = this.alertCtrl.create({
          title: 'Łups',
          subTitle: 'Coś nie bangla z połączeniem, nie mogłem się podłączyć pod doniczkę. Spróbuj jeszcze raz, a jak nie to uruchom ponownie apke i/lub doniczkę, w każdym razie nie dzwoń do mnie :)',
          buttons: ['OK']
        });
        errAlert.present();
      }

    }, err => {
      console.log("Whoops, couldn't connect");
      console.log(err);
      this._stopLoader();

      let errAlert = this.alertCtrl.create({
        title: 'Łups',
        subTitle: 'Coś nie bangla, nie mogłem się podłączyć pod doniczkę. Spróbuj jeszcze raz, a jak nie to uruchom ponownie apke i/lub doniczkę, w każdym razie nie dzwoń do mnie :)',
        buttons: ['OK']
      });
      errAlert.present();
    });
  }

  private _getArchiveData() {
    this.bluetoothSerial.write('GET_MEASUREMENTS').then(() => {
      this._startLoader();
      setTimeout(() => {
        this.bluetoothSerial.readUntil(';').then((data) => {
          if( data != '') {
            this.chartData = this._parseArchiveData(data);
            this._renderChart();
            this._stopLoader();
          } else {
            let errAlert = this.alertCtrl.create({
              title: 'Hmmm',
              subTitle: 'Wylądana na to że nie wszystkie dane się sciągły, kliknij jeszcze raz guziora do pobrania',
              buttons: ['OK']
            });
            errAlert.present();
            this._stopLoader();
          }
        }, err => {
          let errAlert = this.alertCtrl.create({
            title: 'Łups',
            subTitle: 'Powiem apce żeby mi wysłała dane, ale coś się porypało podczas przesyłania tych danych. Standardowo spróbuj wyłączyć i włączyć ponownie :P',
            buttons: ['OK']
          });
          errAlert.present();
          this._stopLoader();
        });
      }, 8000);

    }, err => {
      let errAlert = this.alertCtrl.create({
        title: 'Łups',
        subTitle: 'Coś się wywaliło jak dzwoniłem do urządzenia żeby mi wysłało dane o pomiarach z ostatnich 24h. Spróbuj może apkę jeszcze raz włączyć.',
        buttons: ['OK']
      });
      errAlert.present();
    }); 
  }

  private _renderChart() {
    console.log("chart data:");
    console.log(this.chartData);
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: 'line',
      data: {
        // labels: ["January", "February", "March", "April", "May", "June", "July"],
        labels: this.chartData.map(x => x.date), //.reverse(),
        datasets: [
          {
            label: "Wilgotność (%)",
            fill: true,
            lineTension: 1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.chartData.map(x => parseInt(x.value)), //.reverse(),
            spanGaps: false,
          }
        ],
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: false,
            ticks: {
              beginAtZero:true
            }
          }],
          yAxes: [{
            display: true
          }],
        },
      }

    }); 
  }

  private _startLoader() {
    this._loader = this.loadingCtrl.create({
      content: "Łączym się, robim co mogim ale daj chwilę i nie przeszkadzaj :)",
    });
    this._loader.present();
  }

  private _stopLoader() {
    this._loader.dismiss();
  }

  private _parseArchiveData(data) {
    let records = [];
    let lines = data.split('\r\n'); // split by line
    lines.splice(-1,1); // remove the last element since it's delimiter (';')

    for(let line of lines) {
      let record_obj = line.split('#');
      let record = {'date': record_obj[0], 'value': record_obj[1]};
      records.push(record);
    }

    return records;
  }

}
